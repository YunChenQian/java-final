package inventory.Products;

public class Candy extends Products {

    /**
     * constructor for the Candy
     * @param id
     * @param name
     * @param qty
     * @param price
     * @param type
     * @param flavor
     * @param stock
     */
    public Candy(int id, String name, int qty, double price, String type, String flavor, int stock) {
        super(id, name, qty, price, type, flavor, stock);
    }
    
    public Candy() {}

    //override equals method to return equal if the name, id, type and flavor is the same
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Candy)){
            return false;
        }
        Candy candy = (Candy)o;
        if (candy.getName().equals(this.getName())){
            if(candy.getId() == this.getId()){
                if(candy.getType().equals(this.getType())){
                    if(candy.getFlavor().equals(this.getFlavor())){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
