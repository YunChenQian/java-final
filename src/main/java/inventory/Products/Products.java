package inventory.Products;

import java.sql.*;
import oracle.jdbc.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import inventory.Shops.Shop;

public abstract class Products implements SQLData {
    // name of the type in sql (needs to be ALL CAPS)
    private final String TYPE_NAME = "PRODUCT_TYP";

    private int id;
    private String name;
    private int qty;
    private double price;
    private String type;
    private String flavor;
    private int stock;

    // Empty constructor for SQL
    public Products(){}

    /**
     * constructor for the product abstract class 
     * @param id
     * @param name
     * @param qty quantity of the product IN a BAG (one purchase)
     * @param price
     * @param type
     * @param flavor
     * @param stock
     */
    public Products(int id, String name, int qty, double price, String type, String flavor, int stock) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.price = price;
        this.type = type;
        this.flavor = flavor;
        this.stock = stock;
    }

    
    /**
     * returns the Id of the product
     * @return
     */
    public int getId() {
        return this.id;
    }
    /**
     * returns the name of the product
     * @return
     */
    public String getName() {
        return this.name;
    }
    /**
     * returns the quantity of the product PER BAG
     * @return
     */
    public int getQty() {
        return this.qty;
    }
    /**
     * returns the price of the product
     * @return
     */
    public double getPrice() {
        return this.price;
    }
    /**
     * returns the type of the product
     * @return
     */
    public String getType() {
        return this.type;
    }
    /**
     * returns the flavor of the product
     * @return
     */
    public String getFlavor() {
        return this.flavor;
    }
    /**
     * returns the stock of the product
     * @return
     */
    public int getStock() {
        return this.stock;
    }
    
    /**
     * sets the name of the product
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * sets the quantity of the product PER BAG
     * @param qty
     */
    public void setQty(int qty) {
        this.qty = qty;
    }
    /**
     * sets the price of the product
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }
    /**
     * sets the type of the product
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * sets the flavor of the product
     * @param flavor
     */
    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }
    public void setStock(int stock) {
        this.stock = stock;
    }

    /** 
     * updates the stock with the ammount taken
     * @param ammount amount we would like to change by
     */
    public void updateStock(int ammount) {
        if(this.stock < ammount){
            throw new IllegalArgumentException();
        }
        this.stock += ammount;
    }

    /**
     * to string of product which displays all the information about it
     */
    public String toString() {
        return "ID: " + this.id + "\tName: " + this.name + 
        "\tQty: " + this.qty + "\tPrice: " + this.price + 
        "\tType: " + this.type + "\tFlavor: " + this.flavor + 
        "\tStock " + this.stock;
    }

    // Overrides required to make SQLData work
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getId());
        stream.writeString(getName());
        stream.writeInt(getQty());
        stream.writeDouble(getPrice());
        stream.writeString(getType());
        stream.writeString(getFlavor());
        stream.writeInt(getStock());
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setName(stream.readString());
        setQty(stream.readInt());
        setPrice(stream.readDouble());
        setType(stream.readString());
        setFlavor(stream.readString());
        setStock(stream.readInt());
    }
    
    @Override
    public String getSQLTypeName() throws SQLException {
        return TYPE_NAME;
    }

    /**
     * Loads all products from SQL and puts them in the corresponding shops
     * @param con
     * @param shops
     * @return product list
     */
    public List<Products> loadAllProducts(Connection con, List<Shop> shops) {
        List<Products> products = new ArrayList<>();
        try {
            Map map = con.getTypeMap();
            con.setTypeMap(map);

            map.put(TYPE_NAME, Class.forName("inventory.Products.Products"));

            String call_displayallproduct = "{call javastorepackage.loadallproduct(?)}";
            try(CallableStatement stmt = con.prepareCall(call_displayallproduct)) {
                stmt.registerOutParameter(1, OracleTypes.CURSOR);
                stmt.executeQuery();
                ResultSet rs = (ResultSet)stmt.getObject(1);
                while(rs.next()) {
                    if (rs.getString("objectType").equals("candy")) {
                        products.add(new Candy(
                            rs.getInt("productID"), 
                            rs.getString("name"), 
                            rs.getInt("quantity"), 
                            rs.getDouble("price"), 
                            rs.getString("type"), 
                            rs.getString("flavor"), 
                            rs.getInt("stock")));
                    }
                    else if (rs.getString("objectType").equals("icecream")) {
                        products.add(new IceCream(
                            rs.getInt("productID"), 
                            rs.getString("name"), 
                            rs.getInt("quantity"), 
                            rs.getDouble("price"), 
                            rs.getString("type"), 
                            rs.getString("flavor"), 
                            rs.getInt("stock")));
                    }
                    // Automatically load into shops as well
                    for (Shop shop : shops) {
                        if (shop.getShopName().toLowerCase().equals(rs.getString("shopName").toLowerCase())) {
                            shop.addItem(products.get(products.size() - 1));
                        }
                    }   
                }
            }
            
            catch(SQLException e) {
                System.out.println(e);
                e.printStackTrace();
            } 
        }
        catch(SQLException e) {
            System.out.println(e);
            e.printStackTrace();
        }
        catch(ClassNotFoundException e) {
            System.out.println(e);
            e.printStackTrace();
        } 
        return products;
    }
}
