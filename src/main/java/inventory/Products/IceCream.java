package inventory.Products;

public class IceCream extends Products{

    /**
     * constructor for creating an IceCream
     * @param id
     * @param name
     * @param qty
     * @param price
     * @param type
     * @param flavor
     * @param stock
     */
    public IceCream(int id, String name, int qty, double price, String type, String flavor, int stock) {
        super(id, name, qty, price, type, flavor, stock);
    }

    //override the equals method to be equal if the name, id, type and flavor is equal
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof IceCream)){
            return false;
        }
        IceCream iceCream = (IceCream)o;
        if (iceCream.getName().equals(this.getName())){
            if(iceCream.getId() == this.getId()){
                if(iceCream.getType().equals(this.getType())){
                    if(iceCream.getFlavor().equals(this.getFlavor())){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    
}
