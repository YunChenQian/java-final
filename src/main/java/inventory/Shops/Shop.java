package inventory.Shops;

import java.util.*;

import inventory.Products.Products;

public class Shop {
    private String name;
    private List<Products> pList;

    /**
     * contructor for creating a shop
     * @param name the name of the shop
     * @param pList the list of the products that will be initialized with the shop
     */
    public Shop(String name, List<Products> pList){
        this.name = name;
        this.pList = pList;
    }

    /**
     * constructor for creating shop with no products yet
     * @param name name of shop
     */
    public Shop(String name){
        this.name = name;
        this.pList = new ArrayList<Products>();
    }

    /**
     * returns the name of the shop
     * @return name of shop
     */
    public String getShopName() {
        return this.name;
    }

    /**
     * returns a price of a product if it exists in the shop
     * @param p the product
     * @return price of the product
     */
    public double getProductPrice(Products p) {
        if(this.pList.contains(p)){
            return p.getPrice();
        }
        else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * returns quantity (per) of a product
     * @param p the product
     * @return  quantity (per) of a product
     */
    public int getProductQty(Products p) {
        if(this.pList.contains(p)){
            return p.getQty();
        }
        else{
            throw new IllegalArgumentException();
        }
        
    }

    /**
     * adds a item to the product list of the shop
     * @param p the product to be added
     */
    public void addItem(Products p) {
        this.pList.add(p);
    }

    /**
     * updates a product with the given amount (minus or plus) throws error if there is no such product
     * @param p the product to be updated
     * @param stock the quantity to be updated by
     */
    public void updateItem(Products p, int stock) {
        int indexOfProduct = pList.indexOf(p);
        if(!(indexOfProduct == -1)){
            pList.get(indexOfProduct).updateStock(stock);
        }
        else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * returns the list of ALL products in the shop
     * @return the list of all products in the shop
     */
    public List<Products> getProducts() {
        return this.pList;
    }
    
    /**
     * gets the stock (how many times the product can be sold) from the shop throws exception if not found
     * @param p product 
     * @return the stock of product
     */
    public int getStock(Products p) {
        int indexOfProduct = pList.indexOf(p);
        if(!(indexOfProduct == -1)){
            return pList.get(indexOfProduct).getStock();
        }
        else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * returns a list of products that matches the flavor and throws exception if there are none
     * @param flavor given flavor
     * @return returns a list of products that matches the flavor
     */
    public List<Products> getProductsFromFlavor(String flavor){
        List<Products> products = new ArrayList<Products>();
        for(int i = 0; i < this.pList.size(); i++){
            if(this.pList.get(i).getFlavor().equals(flavor)){
                products.add(this.pList.get(i));
            }
        }
        if(products.size() == 0){
            throw new IllegalArgumentException();
        }
        return products;
    }

    /**
     * returns a list of products that matches the type and throws exception if there are none
     * @param type given type
     * @return return list of products that matches the type
     */
    public List<Products> getProductsFromType(String type){
        List<Products> products = new ArrayList<Products>();
        for(int i = 0; i < this.pList.size(); i++){
            if(this.pList.get(i).getType().equals(type)){
                products.add(this.pList.get(i));
            }
        }
        if(products.size() == 0){
            throw new IllegalArgumentException();
        }
        return products;
    }

    /**
     * removes an item from the Shop list by the given index
     * @param p
     */
    public void removeProduct(Products p){
        int indexOfProduct = pList.indexOf(p);
        if(!(indexOfProduct == -1)){
            pList.remove(indexOfProduct);
        }
        else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * gets the product from ID by looping throught the list of products of this shop
     * @param productID
     */
    public Products getProductFromID(int productID){
        for (Products p : this.getProducts()) {
            if (p.getId() == productID) {
                return p;
            }
        }
        return null;
    }
}
