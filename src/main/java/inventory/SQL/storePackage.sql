CREATE OR REPLACE PACKAGE JAVASTOREPACKAGE AS
--procedures to add an object to a table
TYPE mycursor IS REF CURSOR;

PROCEDURE loadallproduct(returncursor IN OUT mycursor);

END JAVASTOREPACKAGE;

/

CREATE OR REPLACE PACKAGE BODY JAVASTOREPACKAGE AS


--displays all products of product table
PROCEDURE loadallproduct(returncursor IN OUT mycursor) 
AS
BEGIN
    open returncursor for
        SELECT * FROM product;
END;


END JAVASTOREPACKAGE;