DROP TABLE Products;

-- Part 1 Creating the tables
CREATE TABLE Products (
    objectType varchar2(80) NOT NULL,
    productID number(10,0) NOT NULL,
    name varchar2(80) NOT NULL,
    quantity number(10,0) NOT NULL,
    price number(10,2) NOT NULL,
    type varchar2(80) NOT NULL,
    flavor varchar2(80) NOT NULL,
    stock number(10,0) NOT NULL,
    shopName varchar2(80)
    
);

-- Part 2 Grant access to Chen
INSERT INTO Products VALUES ('candy', 1, 'something', 300, 3.00, 'soft candy', 'fruit', 1738, 'chenIceCream');
INSERT INTO Products VALUES ('candy', 2, 'starburst', 200, 1.75, 'soft candy', 'fruit', 1348, 'chenIceCream');
INSERT INTO Products VALUES ('candy', 3, 'skittle', 150, 1.25, 'soft candy', 'fruit', 13483, 'chenIceCream');
INSERT INTO Products VALUES ('candy', 1, 'gummies', 160, 1.00, 'soft candy', 'tropical', 133, 'jojoCandy');
INSERT INTO Products VALUES ('icecream', 2, 'hagendaz', 400, 6.50, 'hard serve', 'vanilla', 44, 'jojoCandy');
