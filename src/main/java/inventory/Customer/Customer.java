package inventory.Customer;

public class Customer {
    private double money;
    private int points;

    /**
     * creates a customer with no money and points
     */
    public Customer() {
        this.money = 0;
        this.points = 0;
    }

    /**
     * returns the money of the customer
     */
    public double getMoney() {
        return this.money;
    }

    /**
     * sets the money of the customer
     */
    public void setMoney(double money) {
        this.money = money;
    }

    /**
     * returns the points of the customer
     */
    public int pointsToCredit() {
        return this.points/20;
    }


    /** 
     * makes the amount of points convert to money for the customer and removes the points from the customer
     * @param points
     */
    public void setPointsToCredit(int points) {
        this.money += points/20; 
        this.points -= points;
    }

    /**
     * gets the points from the customer
     */
    public int getPoints() {
        return this.points;
    }

    /**
     * updates the amount of money that the customer has
     */
    public void updateMoney(double money) {
        this.money += money;
    }

    
    
    /** 
     * adds money to the customer depending on the money given
     * @param money money spent
     * @return int amount of points
     */
    public int addByMoneySpentPoints(double money) {
        this.points += (int)money;
        return (int)money;
    }
}
