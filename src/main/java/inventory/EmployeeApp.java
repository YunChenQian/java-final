package inventory;

import java.io.Console;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import inventory.Customer.Customer;
import inventory.Products.Candy;
import inventory.Products.Products;
import inventory.Shops.Shop;
import inventory.Utils.*;

public class EmployeeApp {
    private static boolean csv = true;
    private static Connection con;
    private static Shop shop;
    private static Customer customer;

    public static void main(String[] args) {
        importMenu();
        storeMenu();
        mainMenuEmployee();

    }

    // Asks the user wether he would like to use csv or sql as the main data source
    public static void importMenu() {
        try {
            String action = Displayer.importMenu();

            if (action.equals("1")) {
                // Set it in csv mode
                csv = true;
                
            }
            else if (action.equals("2")) {
                // Set it in sql mode
                csv = false;
                login();
            }
            else {
                System.out.println("Invalid Action");
                importMenu();
            }
        }catch (Exception e) {
            System.out.println("error");
            importMenu();
        }
        
    }

    /**
     * this method calls the method to print out all the stores and depending on the store order it will choose if the input is good
     */
    public static void storeMenu() {
        try {
            List<Shop> shops;
            if (csv) {
                shops = Displayer.loadProductsIntoShop();
            }
            else {
                shops = Displayer.loadShops();
                Products p = new Candy();
                p.loadAllProducts(con, shops);
            }
            
            String action = Displayer.storeMenu(shops);
            if (Integer.parseInt(action) > 0 && Integer.parseInt(action) < shops.size() + 1){
                shop = shops.get(Integer.parseInt(action) - 1);
                System.out.println(shop.getShopName());
            }
            else {
                System.out.println("Invalid Action");
                storeMenu();
            }
            customer = Displayer.makeCustomer();

        }catch (Exception e) {
            System.out.println("error");
            storeMenu();
        }
        
    }

    /**
     * Main menu after all the setup has been done, this is where the admin or employee will choose what menu they would like to go to next
     */
    public static void mainMenuEmployee() {
        try {
            String action = Displayer.mainMenuEmployee();

            if (action.equals("1")) {
                displayMenu();
            }
            else if (action.equals("2")) {
                Displayer.makeOrder(shop, customer);
                mainMenuEmployee();
            }
            else if (action.equals("3")) {
                Displayer.displayMoneyAndPoints(customer);
                mainMenuEmployee();
            }
            else if (action.equals("0")) {
                System.out.println("Thank you for using");
                System.exit(0);
            }
            else {
                System.out.println("Invalid Action");
                mainMenuEmployee();
            }
        }catch (Exception e) {
            mainMenuEmployee();
        }
    }

    /**
     * display menu for functions with many ways to display a product, like filtering and sorting 
     */
    public static void displayMenu() {
        try{
            String action = Displayer.displayMenu();

            if (action.equals("1")) {
                if (csv) {
                    Displayer.showAllProductsCSV(shop);
                }
                else {
                    // sql would go here
                } 
            }
            else if (action.equals("2")) {
                Displayer.filterProductFlavor(shop);
            }
            else if (action.equals("3")) {
            Displayer.filterProductType(shop);
            }
            else if (action.equals("4")) {
            String sortField = Displayer.userInputSortField();
            boolean sortOrder = Displayer.userInputSortOrder();
            Sorter.sortProductsBy(shop.getProducts(), sortOrder, sortField);
            }
            else if (action.equals("0")) {
                mainMenuEmployee();
            }
            else {
                System.out.println("Invalid Action");
                mainMenuEmployee();
            }
            displayMenu();
        }catch (Exception e) {
            System.out.println("error while displaying please try again");
            displayMenu();
        }
        
    }

    /**
     * Handles login for sql
     */
    public static void login() {
        boolean hasLogin = false;
        Console cnsl = System.console();
        while (hasLogin == false) {
            try {
                String usr = cnsl.readLine("Enter username : ");
                String pwd = String.valueOf(cnsl.readPassword("Enter password : "));
                SQLServices sql = new SQLServices(usr, pwd);
                hasLogin = true;
                con = sql.getConnection();
                System.out.println("Login succesful");
            }
            catch (SQLException e) {
                System.out.println("Invalid login information");
            }

        }
    }
}
