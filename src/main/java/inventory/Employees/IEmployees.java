package inventory.Employees;

import java.util.List;

import inventory.Products.Products;
import inventory.Shops.Shop;

abstract class IEmployees {

    private String username;
    private String password;

    /**
     * constructor for IEmployees
     * @param username
     * @param password
     */
    public IEmployees(String username, String password) {
        this.username = username;
        this.password = password;
    }

    
    /** 
     * gets the product information of the certain shop
     * @param p product given
     * @return String returns the string of the product in shop
     */
    public String getInformationProduct(Products p) {
        return "current product: " + p.getName() + ", id: " + p.getId() + ", price: " + p.getPrice() + ", quantity: " + p.getQty() + ", type: " + p.getType() + ", flavor: " + p.getFlavor() + ", stock: " + p.getStock();
    }

    /**
     * gets the information about the shop: the name and all the product names it has
     * @param s
     * @return returns the string that shows the name of shop and all the product names
     */
    public String getInformationShop(Shop s) {
        List<Products> products = s.getProducts();
        String productString = "";
        for (int i  = 0; i < products.size(); i++) {
            productString += products.get(i).getName() + ", ";
        }
        return ("current shop: " + s.getShopName() + ", products: " + productString);
    }

    //getters
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
}
