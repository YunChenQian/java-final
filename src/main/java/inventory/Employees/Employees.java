package inventory.Employees;

public class Employees extends IEmployees {
    
    /**
     * Constructor for the Employee
     * @param username username for the employee
     * @param password password for the employee
     */
    public Employees(String username, String password) {
        super(username, password);
    }
}
