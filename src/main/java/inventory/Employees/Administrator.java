package inventory.Employees;

import inventory.Products.*;
import inventory.Shops.*;

public class Administrator extends IEmployees {

    /**
     * constructor for administrator
     * @param username
     * @param password
     */
    public Administrator(String username, String password) {
        super(username, password);
    }

    /**
     * adds an item to the given shop
     * @param p product to add
     * @param s shop to add to
     */
    public void addItem(Products p, Shop s) {
        s.addItem(p);
    }


    /**
     * updates the quantity of an item in the given shop
     * @param p product to update quantity
     * @param s shop to update from
     * @param qty quantity to update by
     */
    public void updateItem(Products p, Shop s, int qty) {
        s.updateItem(p, qty);
    }
}
