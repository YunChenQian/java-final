package inventory.Utils;
import java.util.Comparator;

import inventory.Products.Products;

public class NameComparator implements Comparator<Products>{

    /** 
     * compares the two products depending on the name
     * @param one 
     * @param two
     * @return int 
     */
    @Override
    public int compare(Products one, Products two) {
        return one.getName().compareTo(two.getName());
    }
    
}
