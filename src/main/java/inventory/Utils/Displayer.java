package inventory.Utils;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import inventory.Customer.Customer;
import inventory.Products.Candy;
import inventory.Products.IceCream;
import inventory.Products.Products;
import inventory.Readers.IReader;
import inventory.Readers.ReaderCsv;
import inventory.Shops.Shop;

public class Displayer {
    private static Console cnsl = System.console();

    // Asks the user wether he would like to use csv or sql as the main data source
    public static String importMenu() {
        System.out.println("--------------------------------------------");
        System.out.println("Do you want to use CSV or SQL as the main data source?");
        System.out.println("1 - CSV");
        System.out.println("2 - SQL");

        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * prints all the stores in a list type of print
     * @param shops a List of shops to print
     * @return returns a string of the user action
     */
    public static String storeMenu(List<Shop> shops) {
        System.out.println("--------------------------------------------");
        System.out.println("Select Store");
        int numOfShop = 1;
        for (Shop s : shops) {
            System.out.println(numOfShop + " - " + s.getShopName());
            numOfShop++;
        }

        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * prints the main menu
     * @return returns the user input
     */
    public static String mainMenu(boolean csv) {
        System.out.println("--------------------------------------------");
        System.out.println("Main Menu");
        System.out.println("1 - Display Information");
        System.out.println("2 - Make a Order");
        System.out.println("3 - Remind me of how much money and points that I have");
        if (csv) {
            System.out.println("4 - Add Data");
            System.out.println("5 - Update Data");
            System.out.println("6 - Remove Data");
        }
        System.out.println("0 - Exit");
        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * prints the main menu
     * @return returns the user input
     */
    public static String mainMenuEmployee() {
        System.out.println("--------------------------------------------");
        System.out.println("Main Menu");
        System.out.println("1 - Display Information");
        System.out.println("2 - Make a Order");
        System.out.println("3 - Remind me of how much money and points that I have");
        System.out.println("0 - Exit");
        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * prints the display options menu
     * @return returns the user input
     */
    public static String displayMenu() {
        System.out.println("--------------------------------------------");
        System.out.println("Display Information");
        System.out.println("1 - Display All Products");
        System.out.println("2 - Display Products with Flavor");
        System.out.println("3 - Display Products from Type");
        System.out.println("4 - Display Products in Sorted Order");
        System.out.println("0 - Return to Main Menu");

        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * prints the add menu options
     * @return returns the user input
     */
    public static String addMenu() {
        System.out.println("--------------------------------------------");
        System.out.println("Add Data");
        System.out.println("1 - Add Product");
        System.out.println("0 - Return to Main Menu");

        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * prints update menu options
     * @return returns the user input
     */
    public static String updateMenu() {
        System.out.println("--------------------------------------------");
        System.out.println("Update Data");
        System.out.println("1 - Update Product");
        System.out.println("0 - Return to Main Menu");

        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * prints the remove menu options
     * @return returns the user input
     */
    public static String removeMenu() {
        System.out.println("--------------------------------------------");
        System.out.println("Remove Data");
        System.out.println("1 - Delete Order");
        System.out.println("2 - Delete Product");
        System.out.println("0 - Return to Main Menu");

        System.out.print("Enter Desired Action : ");
        return cnsl.readLine();
    }

    /**
     * asks the user what flavor they would like to search for then returns the given input
     * @return
     */
    public static String userInputFlavor() {
        System.out.println("What flavor of candy/ice cream would you like to search for?");
        return cnsl.readLine();
    }

    /**
     * asks the user what type they would like to search for
     * @return the user input
     */
    public static String userInputType() {
        System.out.println("What type of candy/ice cream would you like to search for?");
        return cnsl.readLine();
    }

    /**
     * asks the user what would they like the products to be sorted by
     * @return user input
     */
    public static String userInputSortField() {
        System.out.println("What would you like the products to be sorted by? (flavor, id, name, price, stock, type)");
        return cnsl.readLine();
    }

    /**
     * asks the user if they would like to have it sorted by ascending or descnding 
     * @return user input
     */
    public static boolean userInputSortOrder() {
        System.out.println("In what order would you like the products to be sorted by? (asc|desc)");
        String answer = cnsl.readLine();
        if(answer.equals("asc")) {
            return true;
        }
        else if(answer.equals("desc")) {
            return false;
        }
        else {
            System.out.println("Invalid sorting order. Defaulting to ascending");
            return true;
        }
    }

    /**
     * asks the user for inputs for creating a new product and creates the new product
     * @return a new product with user inputs
     */
    public static Products userInputProduct() {
        System.out.println("Is the product a candy or icecream?");
        String classOfProduct = cnsl.readLine();
        System.out.print("Enter id of product : ");
        String id = cnsl.readLine();
        System.out.print("Enter name of product : ");
        String name = cnsl.readLine();
        System.out.print("Enter quantity of product in grams : ");
        String qty = cnsl.readLine();
        System.out.print("Enter price of product : ");
        String price = cnsl.readLine();
        System.out.print("Enter type of product : ");
        String type = cnsl.readLine();
        System.out.print("Enter flavor of product : ");
        String flavor = cnsl.readLine();
        System.out.print("Enter stock of product : ");
        String stock = cnsl.readLine();
        
        if (classOfProduct.equals("candy")) {
            Candy product = new Candy(
                Integer.parseInt(id), 
                name, 
                Integer.parseInt(qty), 
                Double.parseDouble(price), 
                type, 
                flavor, 
                Integer.parseInt(stock));

            return product;
        }
        else if (classOfProduct.equals("icecream")) {
            IceCream product = new IceCream(
                Integer.parseInt(id), 
                name, 
                Integer.parseInt(qty), 
                Double.parseDouble(price), 
                type, 
                flavor, 
                Integer.parseInt(stock));

            return product;
        }

        return null;
    }
 
    /**
     * asks the user to enter the product Id
     * @return an int representing the productid
     */ 
    public static int userInputProductID() {
        System.out.print("Enter id of product : ");
        String id = cnsl.readLine();
    
        return Integer.parseInt(id);
    }

    /**
     * asks the user to enter a field they would like to modify
     * @return string representing the field name
     */
    public static String userInputModifyField() {
        System.out.println("What would you like to modify? (name, price, qty, type, flavor)");
        return cnsl.readLine();
    }

    /**
     * asks the user to enter a new value
     * @return a string representing the new value
     */
    public static String userInputNewValue() {
        System.out.print("Enter new value : ");
        return cnsl.readLine();
    }

      /**
     * Shows all the products inside the selected store
     */
    public static void showAllProductsCSV(Shop shop) {
        for (Products p : shop.getProducts()) {
            System.out.println(p);
        }
    }

    /**
     * shows all the products inside the selected store with their id, name, cost and stock
     * @param shop
     */
    public static void showProductIdAndName(Shop shop) {
        for (Products p : shop.getProducts()) {
            System.out.println("ProductId: " + p.getId() + " Name: " + p.getName() + " Cost: " + p.getPrice() + "$ Stock: " + p.getStock());
        }
    }

    /**
     * loads the shops from the path
     * @return List of shops
     */
    public static List<Shop> loadShops(){
        List<Shop> allShops = new ArrayList<Shop>();
        try {
            ReaderCsv r = new ReaderCsv();
            allShops = r.loadShops("src/main/java/inventory/Readers/store.csv");
        }catch(IOException e) {
            System.out.println("Error loading");
        }
        return allShops;
    }

    /**
     * adss item into the shop
     * @return List of shops with products in them
     */
    public static List<Shop> loadProductsIntoShop() {
        List<Shop> shops = loadShops();
        try {
            IReader r = new ReaderCsv("src/main/java/inventory/Readers/products.csv");
            r.loadFile();
            r.loadProductsIntoShops(shops);
        }
        catch(IOException e) {
            System.out.println(e);
        } 
        return shops;
    }

    /**
     * filters the products of the given shop by flavor 
     * @param shop current shop
     */
    public static void filterProductFlavor(Shop shop) {
        String flavor = Displayer.userInputFlavor();
        List<Products> productList = shop.getProductsFromFlavor(flavor);
        for (Products product : productList) {
            System.out.println(product);
        }
    }

    /**
     * filters the product by the given type 
     * @param shop current shop
     */
    public static void filterProductType(Shop shop) {
        String type = Displayer.userInputType();
        List<Products> productList = shop.getProductsFromType(type);
        for (Products product : productList) {
            System.out.println(product);
        }
    }

    /**
     * asks the customers if they would like to use points
     * @param customer customer
     */
    public static void customerUsePoints(Customer customer) {
        try {
            System.out.println("Enter the amount of points you would like to use (20pts/1$). You have: " + customer.getPoints() + " points");
            int amount = Integer.parseInt(cnsl.readLine());
            if(amount <= customer.getPoints() && amount >= 0){
                customer.setPointsToCredit(amount);
            }
            else{
                System.out.println("You do not have enough points, please try again");
                customerUsePoints(customer);
            }
            
        } catch (Exception e) {
            System.out.println("You have entered the wrong input, please try again");
        }
        
    }

    /**
     * makes an order with the shop and removes the amount ordered
     * @param shop current shot
     * @param customer current customer
     */
    public static void makeOrder(Shop shop, Customer customer) {
        
        try {
            showProductIdAndName(shop);
            System.out.println("Enter the product ID that you would like to order");
            int productId = Integer.parseInt(cnsl.readLine());
            List<Products> products = shop.getProducts();
            int indexOfProduct = -1;
            for (int i = 0; i < products.size(); i++) {
                if (products.get(i).getId() == productId) {
                    indexOfProduct = i;
                }
            }
            Products chosenProduct = products.get(indexOfProduct);
            System.out.println("How much product would you like to buy?");
            int buyAmount = Integer.parseInt(cnsl.readLine());
            if(buyAmount <= chosenProduct.getStock()){
                Double totalCost = chosenProduct.getPrice() * buyAmount;
                Double totalCustomerMoney = customer.getMoney() + customer.pointsToCredit();
                if (!(totalCustomerMoney <= totalCost)) {
                    customerUsePoints(customer);
                    chosenProduct.updateStock(-buyAmount);
                    System.out.println("Order:" + buyAmount + " of " + chosenProduct.getName());
                    System.out.println("points earned: " + customer.addByMoneySpentPoints(totalCost));
                    System.out.println("Total points: " + customer.getPoints());
                    customer.updateMoney(-totalCost);
                }else{
                    System.out.println("You do not have enough money");
                    makeOrder(shop, customer);
                }
            }else{
                System.out.println("There is not enough stock");
                makeOrder(shop, customer);
            }
        } catch (Exception e) {
            System.out.println("You've entered the wrong input, please try again");
            makeOrder(shop, customer);
        }
    }

    /**
     * creates a new customer and asks them how much money they have 
     * @return A new customer
     */
    public static Customer makeCustomer() {
        Customer customer = new Customer();
        try {
            System.out.println("how much money do you have? $");
            Double money = Double.parseDouble(cnsl.readLine());
            customer.setMoney(money);
        } catch (Exception e) {
            System.out.println("Please enter a correct amount");
            makeCustomer();
        }
        return customer;
    }

    /**
     * displays the current customer's money and points
     * @param customer
     */
    public static void displayMoneyAndPoints(Customer customer) {
        System.out.println("You have: " + customer.getMoney() + "$ ,Points: " + customer.getPoints());
    }

    // public static void loadProductFromSQL(List<Shop> shops) {
    //     Products p;
    //     p.loadAllProducts(con, shops);

    // }
}