package inventory.Utils;

import java.util.Comparator;

import inventory.Products.Products;

public class FlavourComparator implements Comparator<Products> {

    /** 
     * compares the two products depending on the flavor
     * @param one 
     * @param two
     * @return int 
     */
    @Override
    public int compare(Products one, Products two) {
        return one.getFlavor().compareTo(two.getFlavor());
    }
    
}
