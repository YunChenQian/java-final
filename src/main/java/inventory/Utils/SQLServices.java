package inventory.Utils;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class SQLServices {
    private final String url = "jdbc:oracle:thin:@198.168.52.211: 1521/pdbora19c.dawsoncollege.qc.ca";
    
    private Connection con;

    /**
     * Initializes connection
     */
    public SQLServices(String usr, String pwd) throws SQLException {
        con = DriverManager.getConnection(url, usr, pwd);
    }

    /**
     * Closes connection
     */
    public void close() {
        try {
            con.close();
            System.out.println("closed connection");
        }
        catch(SQLException e) {System.out.println(e);}
    }

    public Connection getConnection() {
        return this.con;
    }

}
