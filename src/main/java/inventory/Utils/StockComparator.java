package inventory.Utils;
import java.util.Comparator;

import inventory.Products.Products;

public class StockComparator implements Comparator<Products> {

    /** 
     * compares the two products depending on the stock
     * @param one 
     * @param two
     * @return int 
     */
    @Override
    public int compare(Products one, Products two) {
        return one.getStock() - two.getStock();
    }
    
}
