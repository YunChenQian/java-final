package inventory.Utils;
import java.util.Comparator;

import inventory.Products.Products;

public class TypeComparator implements Comparator<Products>{

    /** 
     * compares the two products depending on the type
     * @param one 
     * @param two
     * @return int 
     */
    @Override
    public int compare(Products one, Products two) {
        return one.getType().compareTo(two.getType());
    }
    
}
