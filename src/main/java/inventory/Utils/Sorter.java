package inventory.Utils;

import java.util.*;
import inventory.Products.*;
import inventory.Utils.*;

public class Sorter {

    
    /** 
     * method that takes in a list of products and boolean ascending and a type then calls methods to sort them depending on the type
     * @param products a list of products to sort
     * @param ascending boolean indicating whether to sort by ascending or descending
     * @param type the type we want to sort by (name, price, flavor, id, stock, type)
     */
    public static void sortProductsBy(List<Products> products, boolean ascending, String type){
        if(type.equals("flavor")){
            sortFlavours(products, ascending);
        }
        if(type.equals("id")){
            sortId(products, ascending);
        }
        if(type.equals("name")){
            sortName(products, ascending);
        }
        if(type.equals("price")){
            sortPrice(products, ascending);
        }
        if(type.equals("stock")){
            sortStock(products, ascending);
        }
        if(type.equals("type")){
            sortType(products, ascending);
        }
    }

    /**
     * sorts the List by flavor
     * @param products List to sort
     * @param ascending asc or not
     */
    private static void sortFlavours(List<Products> products, boolean ascending){
        Collections.sort(products, new FlavourComparator());
        if(!ascending){
            Collections.reverse(products);
        }
        for (int i = 0; i < products.size(); i++){
            System.out.println(products.get(i));
        }
    }

    /**
     * sorts the List by ids
     * @param products list to sort
     * @param ascending sort by ascending or not
     */
    private static void sortId(List<Products> products, boolean ascending){
        Collections.sort(products, new IdComparator());
        if(!ascending){
            Collections.reverse(products);
        }
        for (int i = 0; i < products.size(); i++){
            System.out.println(products.get(i));
        }
    }

    /**
     * sorts the list by names
     * @param products list to sort
     * @param ascending sort by ascending or not
     */
    private static void sortName(List<Products> products, boolean ascending){
        Collections.sort(products, new NameComparator());
        if(!ascending){
            Collections.reverse(products);
        }
        for (int i = 0; i < products.size(); i++){
            System.out.println(products.get(i));
        }
    }

    /**
     * sorts the list by price
     * @param products list to sort
     * @param ascending sort by ascending or not
     */
    private static void sortPrice(List<Products> products, boolean ascending){
        Collections.sort(products, new PriceComparator());
        if(!ascending){
            Collections.reverse(products);
        }
        for (int i = 0; i < products.size(); i++){
            System.out.println(products.get(i));
        }
    }

    /**
     * sorts the products by their stock
     * @param products list to sort
     * @param ascending sort by ascending or not
     */
    private static void sortStock(List<Products> products, boolean ascending){
        Collections.sort(products, new StockComparator());
        if(!ascending){
            Collections.reverse(products);
        }
        for (int i = 0; i < products.size(); i++){
            System.out.println(products.get(i));
        }
    }

    /**
     * sorts the product by their type
     * @param products List of products
     * @param ascending sort by ascending or not
     */
    private static void sortType(List<Products> products, boolean ascending){
        Collections.sort(products, new TypeComparator());
        if(!ascending){
            Collections.reverse(products);
        }
        for (int i = 0; i < products.size(); i++){
            System.out.println(products.get(i));
        }
    }
}
