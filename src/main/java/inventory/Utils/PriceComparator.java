package inventory.Utils;
import java.util.Comparator;

import inventory.Products.Products;

public class PriceComparator implements Comparator<Products>{

    
    /** 
     * compares the two products depending on the price
     * @param one 
     * @param two
     * @return int 
     */
    @Override
    public int compare(Products one, Products two) {
        return Double.compare(one.getPrice(), two.getPrice());
    }
    
}
