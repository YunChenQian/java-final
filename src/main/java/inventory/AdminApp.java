package inventory;

import java.io.Console;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import inventory.Customer.Customer;
import inventory.Products.Candy;
import inventory.Products.Products;
import inventory.Readers.ReaderCsv;
import inventory.Shops.Shop;
import inventory.Utils.*;

public class AdminApp {
    private static boolean csv = true;
    private static Connection con;
    private static Shop shop;
    private static Customer customer;

    public static void main(String[] args) {
        importMenu();
        storeMenu();
        mainMenu();
        
    }

    // Asks the user wether he would like to use csv or sql as the main data source
    public static void importMenu() {
        try {
            String action = Displayer.importMenu();

            if (action.equals("1")) {
                // Set it in csv mode
                csv = true;
            }
            else if (action.equals("2")) {
                // Set it in sql mode
                csv = false;
                login();
            }
            else {
                System.out.println("Invalid Action");
                importMenu();
            }
        }catch (Exception e) {
            System.out.println(e);
            importMenu();
        }
        
    }

    /**
     * this method calls the method to print out all the stores and depending on the store order it will choose if the input is good
     */
    public static void storeMenu() {
        try {
            List<Shop> shops;
            if (csv) {
                shops = Displayer.loadProductsIntoShop();
            }
            else {
                shops = Displayer.loadShops();
                Products p = new Candy();
                p.loadAllProducts(con, shops);
            }
            
            String action = Displayer.storeMenu(shops);
            if (Integer.parseInt(action) > 0 && Integer.parseInt(action) < shops.size() + 1){
                shop = shops.get(Integer.parseInt(action) - 1);
                System.out.println(shop.getShopName());
            }
            else {
                System.out.println("Invalid Action");
                storeMenu();
            }
            customer = Displayer.makeCustomer();

        }catch (Exception e) {
            System.out.println("error");
            storeMenu();
        }
        
    }

    /**
     * Main menu after all the setup has been done, this is where the admin or employee will choose what menu they would like to go to next
     */
    public static void mainMenu() {
        try {
            String action = Displayer.mainMenu(csv);

            if (action.equals("1")) {
                displayMenu();
            }
            else if (action.equals("2")) {
                Displayer.makeOrder(shop, customer);
                mainMenu();
            }
            else if (action.equals("3")) {
                Displayer.displayMoneyAndPoints(customer);
                mainMenu();
            }
            else if (csv) {
                if (action.equals("4")) {
                addMenu();
                }
                else if (action.equals("5")) {
                    updateMenu();
                }
                else if (action.equals("6")) {
                    removeMenu();
                }
            }
            else if (action.equals("0")) {
                System.out.println("Thank you for using");
                System.exit(0);
            }
            else {
                System.out.println("Invalid Action");
                mainMenu();
            }
        }catch (Exception e) {
            mainMenu();
        }
    }

    /**
     * display menu for functions with many ways to display a product, like filtering and sorting 
     */
    public static void displayMenu() {
        try{
            String action = Displayer.displayMenu();

            if (action.equals("1")) {
                Displayer.showAllProductsCSV(shop);
                
            }
            else if (action.equals("2")) {
                Displayer.filterProductFlavor(shop);
            }
            else if (action.equals("3")) {
                Displayer.filterProductType(shop);
            }
            else if (action.equals("4")) {
                String sortField = Displayer.userInputSortField();
                boolean sortOrder = Displayer.userInputSortOrder();
                Sorter.sortProductsBy(shop.getProducts(), sortOrder, sortField);
            }
            else if (action.equals("0")) {
                mainMenu();
            }
            else {
                System.out.println("Invalid Action");
                mainMenu();
            }
            displayMenu();
        }catch (Exception e) {
            System.out.println("error while displaying please try again");
            displayMenu();
        }
        
    }

    /**
     * Menu to add a product to the shop and CSV file
     */
    public static void addMenu() {
        try {
            String action = Displayer.addMenu();

            if (action.equals("1")) {
                Products product = Displayer.userInputProduct();
                ReaderCsv r = new ReaderCsv("src/main/java/inventory/Readers/products.csv");
                try {
                    r.writeProduct(product, shop);
                }
                catch (IOException e) {
                    System.out.println("Error writing");
                }
            }
            else if (action.equals("0")) {
            mainMenu();
            }
            else {
                System.out.println("Invalid Action");
                mainMenu();
            }   
            mainMenu();
        } catch (Exception e) {
            System.out.println("error while adding please try again");
            addMenu();
        }
        
    }

    /**
     * menu to update the CSV file and product
     */
    public static void updateMenu() {
        try {
            String action = Displayer.updateMenu();

            if (action.equals("1")) {
                Displayer.showProductIdAndName(shop);
                int productID = Displayer.userInputProductID();
                Products p = shop.getProductFromID(productID);
                String field = Displayer.userInputModifyField();
                String newVal = Displayer.userInputNewValue();
                ReaderCsv r = new ReaderCsv("src/main/java/inventory/Readers/products.csv");
                try {
                    r.modifyProduct(p, shop, field, newVal);
                }
                catch (IOException e) {
                    System.out.println("Error writing");
                }

                }
            else if (action.equals("0")) {
                System.out.println("going to main menu");
            }
            else {
                System.out.println("Invalid Action");
            }
            mainMenu();
        } catch (Exception e) {
            System.out.println("problem while update menu please try again");
            updateMenu();
        }
        
        
    }

    /**
     * menu to remove a product
     */
    public static void removeMenu() {
        try {
            String action = Displayer.removeMenu();

            if (action.equals("1")) {
                
            }
            else if (action.equals("2")) {
                Displayer.showProductIdAndName(shop);
                int productID = Displayer.userInputProductID();

                ReaderCsv r = new ReaderCsv("src/main/java/inventory/Readers/products.csv");
                try {
                    r.deleteProduct(productID, shop);
                }
                catch (IOException e) {
                    System.out.println("Error writing");
                }
            }
            else if (action.equals("0")) {
                mainMenu();
            }
            else {
                System.out.println("Invalid Action");
                mainMenu();
            }
        } catch (Exception e) {
            System.out.println("error while removing please try again");
            removeMenu();
        }
        
    }

    /**
     * Handles login for sql
     */
    public static void login() {
        boolean hasLogin = false;
        Console cnsl = System.console();
        while (hasLogin == false) {
            try {
                String usr = cnsl.readLine("Enter username : ");
                String pwd = String.valueOf(cnsl.readPassword("Enter password : "));
                SQLServices sql = new SQLServices(usr, pwd);
                hasLogin = true;
                con = sql.getConnection();
                System.out.println("Login succesful");
            }
            catch (SQLException e) {
                System.out.println("Invalid login information");
            }

        }
    }
}
