package inventory.Readers;

import java.io.IOException;
import java.util.List;

import inventory.Products.Products;
import inventory.Shops.Shop;

public interface IReader {
    public List<String> loadFile() throws IOException;
    public List<Products> loadProducts() throws IOException;
    public List<Shop> loadProductsIntoShops(List<Shop> shops);
}
