package inventory.Readers;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

import inventory.Products.*;
import inventory.Shops.Shop;

public class ReaderCsv implements IReader {
    private String path;
    private List<String> lines;

    /**
     * constructor for the CSV reader
     * @param path path to the file that needs reading
     */
    public ReaderCsv(String path) {
        this.path = path;
    }
    public ReaderCsv(){}

    /**
     * reads and loads the files read into the allLines field of the reader
     */
    public List<String> loadFile() throws IOException {
        Path p = Paths.get(path);
        List<String> allLines = Files.readAllLines(p);
        this.lines = allLines;
        return allLines;
    }
    
    /**
     * returns a List of Strings from the path given
     * @param path path to file
     * @return List of Strings containing all lines from file
     * @throws IOException
     */
    public List<String> loadFile(Path path) throws IOException {
        List<String> allLines = Files.readAllLines(path);
        return allLines;
    }
    
    /**
     * returns a List of shops after being called
     * @String path to the shop csv
     * @return list of shops
     * @throws IOException
     */
    public List<Shop> loadShops(String pathString) throws IOException {
        Path path = Paths.get(pathString);
        List<String> allShops = loadFile(path);
        List<Shop> shops = new ArrayList<Shop>();
        for (String line : allShops) {
            String[] fields = line.split(",");
            shops.add(new Shop(fields[0])); 
        }
        return shops;
    }
    
    /**
     * Takes a List<String> of lines and returns a List<Products> of products created from it
     * @param lines
     * @return A list of products
     * @throws IOException
     */
    public List<Products> loadProducts() throws IOException {
        List<Products> products = new ArrayList<Products>();
        for (String line : this.lines) {
            String[] fields = line.split(",");
            if (fields[0].equals("candy")) {
                products.add(new Candy(
                    Integer.parseInt(fields[1]), 
                    fields[2], 
                    Integer.parseInt(fields[3]), 
                    Double.parseDouble(fields[4]),
                    fields[5],
                    fields[6],
                    Integer.parseInt(fields[7])));
            }
            else if (fields[0].equals("icecream")) {
                products.add(new IceCream(
                    Integer.parseInt(fields[1]), 
                    fields[2], 
                    Integer.parseInt(fields[3]), 
                    Double.parseDouble(fields[4]),
                    fields[5],
                    fields[6],
                    Integer.parseInt(fields[7])));
            }
        }
        return products;
    }

    
    
    /** 
     * loads all the products from path of the reader into the corresponding shops
     * @param shops list of shops to load products into
     * @return List<Shop> returns list of shops with products in them
     */
    public List<Shop> loadProductsIntoShops(List<Shop> shops) {
        List<String> shopName = new ArrayList<String>();
        for (Shop shop : shops) {
            shopName.add(shop.getShopName());
        }

        for (String line : this.lines) {
            String[] fields = line.split(",");
            int indexOfShop = shopName.indexOf(fields[8]);
            if (fields[0].equals("candy")) {
                Candy candy = new Candy(
                    Integer.parseInt(fields[1]), 
                    fields[2], 
                    Integer.parseInt(fields[3]), 
                    Double.parseDouble(fields[4]),
                    fields[5],
                    fields[6],
                    Integer.parseInt(fields[7]));
                shops.get(indexOfShop).addItem(candy);
                
            }
            else if (fields[0].equals("icecream")) {
                IceCream icecream = new IceCream(
                    Integer.parseInt(fields[1]), 
                    fields[2], 
                    Integer.parseInt(fields[3]), 
                    Double.parseDouble(fields[4]),
                    fields[5],
                    fields[6],
                    Integer.parseInt(fields[7]));

                shops.get(indexOfShop).addItem(icecream);
            }
        }
        return shops;
    }

    /**
     * Takes a Producr and a Shop and adds it to products.csv
     * @param product
     * @param shop
     */
    public void writeProduct(Products product, Shop shop) throws IOException {
        String classOfProduct = product.getClass().getSimpleName().toLowerCase();
        String data = 
            classOfProduct + "," + product.getId() + "," +
            product.getName() + "," + product.getQty() + "," +
            product.getPrice() + "," + product.getType() + "," +
            product.getFlavor() + "," + product.getStock() + "," +
            shop.getShopName();
        
        List<String> lines = this.loadFile();
        lines.add(data);

        Files.write(Paths.get(path), lines);
    }

    /**
     * Takes a productID and a shop and removes a product from product.csv using complicated regex
     * @param productID
     * @param shop
     */
    public void deleteProduct(int productID, Shop shop) throws IOException {
        List<String> lines = this.loadFile();
        
        for (int i = 0; i < lines.size(); i++) {
            String[] id = lines.get(i).split(",");
            String[] shopName = lines.get(i).split(",");
            
            if (id[1].equals(String.valueOf(productID)) && shopName[shopName.length - 1].equals(shop.getShopName())) {
                lines.remove(i);
                shop.removeProduct(shop.getProductFromID(productID)); 
            }
        }

        Files.write(Paths.get(path), lines);
    }
    /**
     * Takes a productID and a shop and removes a product from product.csv using complicated regex
     * @param productID
     * @param shop
     */
    public void modifyProduct(Products product, Shop shop, String type, String newVal) throws IOException {
        List<String> lines = this.loadFile();
       
        if (type.equals("name")) {
            product.setName(newVal);
        }
        else if (type.equals("qty")) {
            product.setQty(Integer.parseInt(newVal));
        }
        else if (type.equals("price")) {
            product.setPrice(Double.parseDouble(newVal));
        }
        else if (type.equals("type")) {
            product.setType(newVal);
        }
        else if (type.equals("flavor")) {
            product.equals(newVal);
        }
        else if (type.equals("stock")) {
            product.equals(Integer.parseInt(newVal));
        }
        
        deleteProduct(product.getId(), shop);
        writeProduct(product, shop);
    }
}
