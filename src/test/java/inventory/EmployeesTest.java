package inventory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import inventory.Employees.Employees;
import inventory.Products.Candy;
import inventory.Products.Products;
import inventory.Shops.Shop;

import java.util.*;


public class EmployeesTest {

    @Test
    public void employeesConstructorTest(){
        Employees e = new Employees("John", "ABC");
        assertEquals("John", e.getUsername());
        assertEquals("ABC", e.getPassword());
    }

    @Test
    public void getInformationProductsTest(){
        Candy c = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Employees e = new Employees("John", "ABC");
        assertEquals("current product: Starburst, id: 1, price: 2.5, quantity: 200, type: Soft Chew, flavor: Fruits, stock: 8", e.getInformationProduct(c));
    }

    @Test
    public void getInformationShopsTest(){
        List<Products> p = new ArrayList<Products>();
        Candy c = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        p.add(c);
        Shop s = new Shop("candy shop", p);
        Employees e = new Employees("John", "ABC");
        assertEquals("current shop: candy shop, products: Starburst, ", e.getInformationShop(s));
    }
}
