package inventory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import inventory.Products.Candy;

public class CandyTest {
    
    @Test
    public void candyConstructorTest()
    {
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals(1, candy.getId());
        assertEquals("Starburst", candy.getName());
        assertEquals(200, candy.getQty());
        assertEquals(2.50, candy.getPrice(), 0);
        assertEquals("Soft Chew", candy.getType());
        assertEquals("Fruits", candy.getFlavor());
        assertEquals(8, candy.getStock());
    }

    @Test
    public void getIdTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals(1, candy.getId());
    }

    @Test
    public void getNameTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals("Starburst", candy.getName());
    }

    @Test
    public void getQtyTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals(200, candy.getQty());
    }

    @Test
    public void getPriceTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals(2.50, candy.getPrice(), 0);
    }

    @Test
    public void getTypeTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals("Soft Chew", candy.getType());
    }

    @Test
    public void getFlavorTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals("Fruits", candy.getFlavor());
    }

    @Test
    public void getStockTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertEquals(8, candy.getStock());
    }

    @Test
    public void updateStockTest(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        candy.updateStock(1);
        assertEquals(9, candy.getStock());
    }

    @Test
    public void equalsTets(){
        Candy candy = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Candy candy1 = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        assertTrue(candy.equals(candy1));
    }
}
