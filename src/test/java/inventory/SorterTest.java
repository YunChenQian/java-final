package inventory;

import java.util.*;

import org.junit.Test;

import inventory.Products.*;
import inventory.Utils.Sorter;

public class SorterTest {
    @Test
    public void SortByFlavor() {
        List<Products> products = new ArrayList<Products>();
        Products product1 = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Products product2 = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        Products product3 = new Candy(2, "highchew", 20, 1.99, "soft", "Licorice", 69420);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        Sorter.sortProductsBy(products, false, "flavor");
    }

    @Test
    public void SortById() {
        List<Products> products = new ArrayList<Products>();
        Products product1 = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Products product2 = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        Products product3 = new Candy(2, "Highchew", 20, 1.99, "soft", "licorice", 69420);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        Sorter.sortProductsBy(products, true, "id");
    }

    @Test
    public void SortByName() {
        List<Products> products = new ArrayList<Products>();
        Products product1 = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Products product2 = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        Products product3 = new Candy(2, "Highchew", 20, 1.99, "soft", "licorice", 69420);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        Sorter.sortProductsBy(products, false, "name");
    }

    @Test
    public void SortByPrice() {
        List<Products> products = new ArrayList<Products>();
        Products product1 = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Products product2 = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        Products product3 = new Candy(2, "highchew", 20, 1.99, "soft", "licorice", 69420);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        Sorter.sortProductsBy(products, false, "price");
    }

    @Test
    public void SortByStock() {
        List<Products> products = new ArrayList<Products>();
        Products product1 = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Products product2 = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        Products product3 = new Candy(2, "highchew", 20, 1.99, "soft", "licorice", 69420);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        Sorter.sortProductsBy(products, false, "stock");
    }

    @Test
    public void SortByType() {
        List<Products> products = new ArrayList<Products>();
        Products product1 = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Products product2 = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        Products product3 = new Candy(2, "highchew", 20, 1.99, "Chewy", "licorice", 69420);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        Sorter.sortProductsBy(products, true, "type");
    }
}
