package inventory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import inventory.Products.IceCream;


public class IceCreamTest {
    
    @Test
    public void iceCreamConstructorTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals(1, iceCream.getId());
        assertEquals("Drumsticks", iceCream.getName());
        assertEquals(400, iceCream.getQty());
        assertEquals(4.75, iceCream.getPrice(), 0);
        assertEquals("Hard Serve", iceCream.getType());
        assertEquals("Caramel", iceCream.getFlavor());
        assertEquals(14, iceCream.getStock());
    }

    @Test
    public void getIdTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals(1, iceCream.getId());
    }

    @Test
    public void getNameTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals("Drumsticks", iceCream.getName());
    }

    @Test
    public void getQtyTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals(400, iceCream.getQty());
    }
    
    @Test
    public void getPriceTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals(4.75, iceCream.getPrice(), 0);
    }

    @Test
    public void getTypeTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals("Hard Serve", iceCream.getType());
    }

    @Test
    public void getFlavorTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals("Caramel", iceCream.getFlavor());
    }

    @Test
    public void getStockTest(){
        IceCream iceCream = new IceCream(1, "Drumsticks", 400, 4.75, "Hard Serve", "Caramel", 14);
        assertEquals(14, iceCream.getStock());
    }

    @Test
    public void updateStockTest(){
        
    }
}
