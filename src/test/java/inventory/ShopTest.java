package inventory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import inventory.Products.Candy;
import inventory.Products.Products;
import inventory.Shops.Shop;

public class ShopTest {
    @Test
    public void candyShop_constructor_test() {
        try {
            List<Products> pList = new ArrayList<>();
            Shop cs = new Shop("gary", pList);
        }
        catch (UnsupportedOperationException e) {
            fail();
        }
    }

    @Test
    public void getName_test() {
        List<Products> pList = new ArrayList<>();
        Shop ics = new Shop("gary", pList);
        assertEquals("gary", ics.getShopName());
    }

    @Test
    public void getPrice_test() {
        List<Products> pList = new ArrayList<>();
        Products p = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        pList.add(p);
        Shop ics = new Shop("gary", pList);
        assertEquals(2.50, ics.getProductPrice(p), 0.01);
    }

    @Test
    public void getQty_test() {
        List<Products> pList = new ArrayList<>();
        Products p = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        pList.add(p);
        Shop ics = new Shop("gary", pList);
        assertEquals(200, ics.getProductQty(p));
    }

    @Test
    public void addItem_test() {
        List<Products> pList = new ArrayList<>();
        Products p = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        Shop ics = new Shop("gary", pList);
        ics.addItem(p);
        // need a getProducts method
        assertEquals(p, ics.getProducts().get(0));
    }

    @Test
    public void updateItem_test() {
        List<Products> pList = new ArrayList<>();
        Products p = new Candy(1, "Starburst", 200, 2.50, "Soft Chew", "Fruits", 8);
        pList.add(p);
        Shop ics = new Shop("gary", pList);
        ics.updateItem(p, 1);
        assertEquals(9, ics.getStock(p));
    }

}
